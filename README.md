# 3D Head Tracking

---

### Summary of project
This project uses 4 key points on users head (in the first stage markers are used as key points) to find it's 3D location with respect to the camera.

The levenberg-marquardt method for optimization is used for finding the location of users head.

The code also generates a 3D scene which reacts to the users movements giving the feeling of a watching a 3D scene.

----
### Results
You can check out the result here:

https://youtu.be/mjisUXKguAc
